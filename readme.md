# unit [![Build Status](https://travis-ci.org/seeessence/unit.svg)](https://travis-ci.org/seeessence/unit) [![Coverage Status](https://coveralls.io/repos/seeessence/unit/badge.svg?branch=master&service=github)](https://coveralls.io/github/seeessence/unit?branch=master)

```bash
npm install seeessence/unit
```

```javascript
var unit = require('unit');

unit({hello:"world"}, 'json decode').pipe(
    json.pipe(
        unit('{"hello":"world"}', 'json encode')
    )
);
```

```bash
node test.js
```

```bash
✔ json encode  
✔ json decode
```
