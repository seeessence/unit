var unit = require('./index.js');
var assert = require('assert');

unit('hello world').pipe(
    unit('hello world'),
    unit(/^hello.?world$/),
    unit('hello world', 'message'),
    unit.not({hello: 'world'}),
    unit.not(null),
    unit(null)
);

unit({hello: 'world'}).pipe(
    unit({hello: 'world'}),
    unit(function(object) {
        assert(object.hasOwnProperty('hello'));
    }),
    unit(function(object, done) {
        setTimeout(done, 1000);
    }),
    unit.not('hello world'),
    unit.not(/^hello.?world$/),
    unit.not('hello world', 'not message')
);
