var stream = require('stream');
var assert = require('assert');
var Mocha = require('mocha');
var inspect = require('util').inspect;

var mocha = new Mocha();

process.once('beforeExit', function () {
    mocha.run();
});

module.exports = function (expected, message, data) {
    
    if (expected instanceof Function ||  typeof expected === 'function') {
        
        var unit = new stream.Duplex({
            objectMode: true,
            read: function () {
                this.push(data);
                this.push(null);
            }
        });
        
        unit.once('pipe', function (source) {
            
            this._write = function(data, encoding, callback) {
                
                mocha.suite.addTest(new Mocha.Test(
                    message || expected.toString(),
                    expected.length === 2
                        ? function (done) {
                              expected.call(this, data, function (error) {
                                  done(error);
                                  callback(error);
                              });
                          }
                        : expected.bind(null, data, callback)
                ));
                
            };
            
            this.pipe(source);
        });
        
        unit.pipe = (function (pipe) {
            
            return function () {
                for (var key in arguments)
                    pipe.call(this, arguments[key], {end: false});
            };
        })(unit.pipe);
        
        return unit;
    }
    
    if (expected instanceof String || typeof expected === 'string')
        
        return module.exports(
            function (object) {
                assert.strictEqual(object, expected);
            },
            message || inspect(expected),
            data || expected
        );
    
    if (expected instanceof RegExp)
        
        return module.exports(
            function (object) {
                assert(expected.test(object));
            },
            message || inspect(expected),
            data || expected
        );
    
    if (expected instanceof Object || typeof callback === 'object')
        
        return module.exports(
            function (object) {
                assert.deepEqual(object, expected);
            },
            message || inspect(expected),
            data || expected
        );
    
    return module.exports(
        assert.bind(null, process.env.npm_package_config_test),
        'not expected'
    );
};

module.exports.not = function (expected, message, data) {
    
    if (expected instanceof String || typeof expected === 'string')
        
        return module.exports(
            function (object) {
                assert.notStrictEqual(object, expected);
            },
            message || 'not \'' + expected + '\'',
            data || expected
        );
    
    if (expected instanceof RegExp)
        
        return module.exports(
            function (object) {
                assert(!expected.test(object));
            },
            message || 'not ' + expected.toString(),
            data || expected
        );
    
    if (expected instanceof Object || typeof callback === 'object')
        
        return module.exports(
            function (object) {
                assert.notDeepStrictEqual(object, expected);
            },
            message || 'not ' + JSON.stringify(expected),
            data || expected
        );
    
    return module.exports(expected);
};
